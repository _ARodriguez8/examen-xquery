"Lista todos los elementos del menú ordenados por nombre de la A a la Z",
for $b in doc("arepazo")//menu
order by $b/nombre ascending
return $b/nombre,
"--------------------------",
"Lista todas las bebidas del menú ordenados por nombre de la Z a la A",
for $b in doc("arepazo")//menu
order by $b/nombre descending
where $b/@tipo="Bebidas"
return $b/nombre,
"---------------------------",
"Lista todos los elementos del menú de tipo Comida y que cuesten mas de 4€",
for $b in doc("arepazo")//menu
where $b/@tipo="Comida" and $b/precio>4
return $b/nombre,
"----------------------------",
"Ordena los postres de mas caro a mas barato. Devuelveme el Nombre, la descripción y el precio de cada uno",
for $b in doc("arepazo")//menu
order by $b/precio descending
where $b/@tipo="Postres"
return ($b/nombre, $b/descripcion, $b/precio),
"----------------------------",
"Lista todos los elementos del menú con 2 o menos ingredientes del tipo Comida.:",
for $b in doc("arepazo")//menu
where $b/@tipo="Comida" and count($b/ingrediente)<3
return ($b/nombre, $b/ingrediente),
"----------------------------",
"Calcula Cuanto hay que cobrar a alguien que pida 5 unidades del ID 7:",
for $b in doc("arepazo")//menu
where $b/@id=7 
return ($b/nombre, $b/precio*5),
"----------------------------",
"Dime cuantos elementos hay en el Menú:",
let $b := doc("arepazo")//menu
return count($b),
"----------------------------",
"Dime cual es el precio  más caro y más barato del menu.:",
let $max := max(doc("arepazo")//menu/precio)
let $min := min(doc("arepazo")//menu/precio)
return <precio>
        <caro>{$max}</caro>
        <barato>{$min}</barato>
       </precio>