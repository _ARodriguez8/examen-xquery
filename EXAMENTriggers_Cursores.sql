--Seguimos con la BBDD del Arepazo.

USE EXAMENRA4

--1.- (2,5 puntos) Haz un trigger para que cuando se a�ada o modifique una linea, se recalcule el subtotal y el total del pedido.
-- recordar que el calculo del subtotal y el total ya lo vimos en el examen del segundo trimestre.
drop trigger recalcular
CREATE TRIGGER recalcular
ON PedidosLinea
FOR INSERT
AS
BEGIN
DECLARE @idPedido INT
DECLARE @cantidad INT
DECLARE @precio INT
SELECT @idPedido=IdPedido, @cantidad=Cantidad, @precio=PrecioVentaUnitario from inserted 
UPDATE Pedidos SET SubTotal=SubTotal+(@cantidad*@precio) where Id=@idPedido
UPDATE Pedidos SET Total=SubTotal+GastosEnvio where Id=@idPedido
END
begin tran
INSERT INTO PedidosLinea VALUES(9,1,1,3.00,4)
rollback tran


--2.- (2,5 puntos) haz una funci�n con un cursor que te devuelva una cadena de texto con todos los elementos no rentables del men� arepazo.
--Recuerda que un elemento es no rentable, cuando sus incredientes cuestan m�s que el precio tal y como vimos en el examen del 2o trimestre.
DECLARE elementosNoRentables CURSOR FOR SELECT m.Id, m.Nombre, m.PrecioVenta, SUM(r.CantidadEnKg*i.PrecioKg) as Coste FROM MENU m INNER JOIN Receta r ON r.idMenu=m.Id 
INNER JOIN Ingredientes i ON r.idIngredientes=i.Id GROUP BY m.id, m.nombre,m.PrecioVenta HAVING SUM(r.CantidadEnKg*i.PrecioKg)>m.precioVenta
OPEN elementosNoRentables
DECLARE @nombre VARCHAR(200)
DECLARE @id INT
DECLARE @precioventa VARCHAR(100)
DECLARE @coste VARCHAR(100)
FETCH NEXT FROM elementosNoRentables INTO @id, @nombre, @precioventa,@coste
WHILE @@FETCH_STATUS=0
	BEGIN
		PRINT @nombre+ ' no es rentable'
		FETCH NEXT FROM elementosNoRentables INTO @id, @nombre, @precioventa,@coste
	END
CLOSE elementosNoRentables
DEALLOCATE elementosNoRentables

--3 (2,5 puntos) A�ade un trigger que te recalcule el gasto de envio al insertar un pedido seg�n el codigo postal. 
--Recordad la funci�n que creamos en clase para calcular los gastos de envio
-- Si el codigo postal es 410NN ser�n 3,99�
-- si el codigo postal es 41NNN ser�n 5,99�
-- para el resto de casos 9,99�

DROP TRIGGER calculaGastosEnvio
CREATE TRIGGER calculaGastosEnvio
ON Pedidos
FOR INSERT
AS BEGIN
DECLARE @cp VARCHAR(100)
DECLARE @gastosEnvio DECIMAL(18,2)
SELECT @cp=Cod_Postal FROM inserted
UPDATE Pedidos SET GastosEnvio=dbo.calcularGastoEnvio(@cp)
END
BEGIN TRAN
INSERT INTO Pedidos VALUES(2021-06-02,0,0,0,5,'636745198','C/Perico Delgado','41210','Guillena')
ROLLBACK TRAN
--AQUI TE DEJO EL CODIGO DE LA FUNCION calcularGastosEnvio
create function dbo.calcularGastoEnvio(@codigoPostal as varchar(50))
returns real
as
begin
declare @resultado as real
if ISNUMERIC(@codigoPostal)=0 or LEN(@codigoPostal)!=5
	set @resultado=null
else if ISNUMERIC(@codigoPostal)=1
	if LEFT(@codigoPostal,3)=410
		set @resultado=3.99
	else if LEFT(@codigoPostal,2)=41
		set @resultado=5.99
	else if LEN(@codigoPostal)=5
		set @resultado=9.99
return @resultado
end
--4 (2,5 puntos) haced una funcion que devuelva la lista de emails de los clientes de un determinado codigo postal. A la funci�n le 
-- pasaremos un c�digo postal y deber� devolvernos una cadena de texto con los emails separados por ,
CREATE FUNCTION filtarEmails(@codpos VARCHAR(100))
RETURNS VARCHAR(8000)
AS BEGIN
	DECLARE @lista VARCHAR(8000)=''
	DECLARE @id INT
	DECLARE @email VARCHAR(200)
	DECLARE getEmail CURSOR FOR SELECT c.Id, c.Email FROM Clientes c WHERE Cod_Postal=@codpos
	OPEN getEmail
	FETCH NEXT FROM getEmail INTO @id, @email
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @lista=@email+' , '
		FETCH NEXT FROM getEmail INTO @id,@email
	END
	CLOSE getEmail
	DEALLOCATE getEmail
	RETURN @lista
END
select * from Clientes WHERE Cod_Postal='41033'
SELECT dbo.filtarEmails('41033')